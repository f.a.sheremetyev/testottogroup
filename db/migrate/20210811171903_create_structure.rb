class CreateStructure < ActiveRecord::Migration[6.1]
  def change
    create_table :clients do |t|
      t.integer :client_num
      t.string :name
      t.string :surname
      t.integer :postcode

      t.timestamps
    end

    create_table :orders do |t|
      t.belongs_to :client

      t.timestamps
    end

    create_table :items do |t|
      t.belongs_to :order
      t.integer :item_num
      t.integer :quantity

      t.timestamps
    end
  end
end
