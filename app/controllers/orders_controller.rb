class OrdersController < ApplicationController
  def index
    @orders = Order.all
  end

  def show
    @order = Order.find(params[:id])
  end

  def new
    @order = Order.new
    @order.client = Client.new
    5.times { @order.items.new }
  end

  def create
    # Создать копию параметров, очищенную от пустых элементов в :items_attributes
    clean_params = {}
    clean_params[:items_attributes] = order_params[:items_attributes].select{|_, val| val[:item_num].present? &&
                                                                                      val[:quantity].present?}
    clean_params[:client_attributes] = order_params[:client_attributes]
    @order = Order.new(clean_params)

    # Найти клиента, затем - проверить, что все данные совпадают
    client = Client.find_by(client_num: @order.client.client_num)
    if client.present?
      validate_client(client, clean_params[:client_attributes])
      # Проставить в заказ существующего клиента, если всё в порядке
      if @order.errors.empty?
        @order.client = client
        # Проверить, что заказ ещё не был сделан
        validate_order_duplication
      end
    end

    if @order.errors.empty? && @order.save
      redirect_to @order
    else
      render :new
    end
  end

  private
  def order_params
    params.require(:order).permit(
      client_attributes: [:client_num, :name, :surname, :postcode ],
      items_attributes: [:item_num, :quantity]
    )
  end

  # Если клиент существует - проверить, что введенные данные совпадают
  def validate_client(client, client_data)
    if client.name != client_data[:name]
      @order.errors.add :base, message: "Имя не совпадает с номером!"
    end
    if client.surname != client_data[:surname]
      @order.errors.add :base, message: "Фамилия не совпадает с номером!"
    end
    if client.postcode != client_data[:postcode].to_i
      @order.errors.add :base, message: "Индекс не совпадает с номером!"
    end
  end

  # Проверить, что заказ не повторяет существующий
  def validate_order_duplication
    # Выбрать все заказы, сделанные клиентом за последнюю неделю
    # Проверить, есть ли среди них заказы, совпадающие по всем позициям (вне зависимости от порядка позиций)
    # Если да - выдать ошибку.
    if @order.client.orders.map.select{|order| order.created_at > Time.now - 1.week}
                               .any? do |order| order.items.map{|item| [item.item_num, item.quantity]} ==
                                               @order.items.map{|item| [item.item_num, item.quantity]}
                               end
      @order.errors.add :base, message: "Этот заказ уже создан!"
    end
  end
end
