class Order < ApplicationRecord
  belongs_to :client
  accepts_nested_attributes_for :client

  has_many :items
  accepts_nested_attributes_for :items
end
