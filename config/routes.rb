Rails.application.routes.draw do
  root "orders#index"

  resources :orders
  # get "/orders", to: "orders#index"
  get "/clients", to: "clients#index"
end
